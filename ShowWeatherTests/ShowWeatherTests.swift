//
//  ShowWeatherTests.swift
//  ShowWeatherTests
//
//  Created by Apit on 8/7/18.
//  Copyright © 2018 Apit. All rights reserved.
//

import XCTest
@testable import ShowWeather

class ShowWeatherTests: XCTestCase {
    
    func getCurrentWeather() {
        let parameter: [String: Any] = ["apikey": Constant.apiKey,
                                        "language": "en-US",
                                        "detail": "true",
                                        "metric": "true"]
        guard case is Error = Constant.GET(url: Constant.API.dailyForecase + "208977", header: [:], parameter: parameter, success: { (json) in
            
        }, failure: { (error) in
            
        }) else {
            XCTFail("Error get data")
            return
        }
    }
    
}
