//
//  SearchCityViewController.swift
//  ShowWeather
//
//  Created by Apit on 8/8/18.
//  Copyright © 2018 Apit. All rights reserved.
//

import UIKit

class SearchCityViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var cities = [City]()
    var parentController: MainWeatherViewController!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CityCell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    func loadCity(textInput: String) {
        let parameter: [String: Any] = ["apikey": Constant.apiKey,
                                        "q": textInput,
                                        "language": "en-US"]
        Constant.GET(url: Constant.API.autoCompleteCity, header: [:], parameter: parameter, success: { (json) in
            self.cities = [City]()
            for cityData in json.arrayValue {
                let city = City()
                if cityData["Country"]["ID"].stringValue == "ID" {
                    city.cityId = cityData["Key"].stringValue
                    city.cityName = cityData["LocalizedName"].stringValue
                    city.region = cityData["AdministrativeArea"]["LocalizedName"].stringValue
                    self.cities.append(city)
                }
            }
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        loadCity(textInput: searchText)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        cell.textLabel?.text = cities[indexPath.row].cityName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        parentController.city = cities[indexPath.row]
        navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
