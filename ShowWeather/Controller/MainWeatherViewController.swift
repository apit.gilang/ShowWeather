//
//  MainWeatherViewController.swift
//  ShowWeather
//
//  Created by Apit on 8/7/18.
//  Copyright © 2018 Apit. All rights reserved.
//

import UIKit

class MainWeatherViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var weathers = [Weather]()
    var city: City!
    var cityKey = ""
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var minMaxTemperatureLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if city != nil {
            cityKey = city.cityId!
        }else {
            cityKey = "208977"
        }
        setUpNavigation()
        loadData()
    }
    
    func loadData() {
        let parameter: [String: Any] = ["apikey": Constant.apiKey,
                         "language": "en-US",
                         "detail": "true",
                         "metric": "true"]
        Constant.GET(url: Constant.API.dailyForecase + cityKey, header: [:], parameter: parameter, success: { (json) in
            self.weathers = [Weather]()
            for data in json["DailyForecasts"].arrayValue {
                let weather = Weather()
                weather.temperature = data["Temperature"]["Maximum"]["UnitType"].intValue
                weather.maxTemperature = data["Temperature"]["Maximum"]["Value"].floatValue
                weather.minTemperature = data["Temperature"]["Minimum"]["Value"].floatValue
                weather.dateTime = data["EpochDate"].intValue
                self.weathers.append(weather)
            }
            if self.weathers.count > 0 {
                self.prepareForUI(index: 0)
            }else {
                let alert = UIAlertController(title: "", message: "Service Unavailable", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    self.cityKey = "208977"
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
            print(json)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func prepareForUI(index: Int) {
        let weather = weathers[index]
        temperatureLabel.text = degreeFormatter(degree: weather.temperature)
        minMaxTemperatureLabel.text = "\(degreeFormatter(degree: Int(weather.minTemperature)))/\(degreeFormatter(degree: Int(weather.maxTemperature)))"
        let date = Date(timeIntervalSince1970: Double(weather.dateTime))
        dateLabel.text = dateFormatter(date: date, source: "date")
        timeLabel.text = dateFormatter(date: date, source: "time")
        tableView.reloadData()
    }

    func setUpNavigation() {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear
        
        let locationLabel = UILabel()
        var stringCity = ""
        if city != nil {
            stringCity = "\(city.cityName.capitalized),"
        }else {
            stringCity = "BANDUNG,"
        }
        locationLabel.text = stringCity
        locationLabel.textColor = UIColor.white
        locationLabel.font = UIFont.boldSystemFont(ofSize: 15)
        locationLabel.sizeToFit()
        
        let regionLabel = UILabel()
        var regionString = ""
        if city != nil {
            regionString = city.region.capitalized
        }else {
            regionString = "WEST JAVA"
        }
        regionLabel.text = regionString
        regionLabel.textColor = UIColor.white
        regionLabel.font = UIFont.systemFont(ofSize: 12)
        regionLabel.sizeToFit()
        
        let location = UIBarButtonItem(customView: locationLabel)
        let region = UIBarButtonItem(customView: regionLabel)
        navigationItem.leftBarButtonItems = [location, region]
        
        let addLocationButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        addLocationButton.setBackgroundImage(UIImage(named: "add-location"), for: .normal)
        addLocationButton.addTarget(self, action: #selector(addLocation(sender:)), for: .touchUpInside)
        let addLocationView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        addLocationView.addSubview(addLocationButton)
        let addLocationBar = UIBarButtonItem(customView: addLocationView)
        navigationItem.rightBarButtonItem = addLocationBar
    }
    
    func degreeFormatter(degree: Int) -> String {
        let tempString = String(degree) + "&deg;"
        let result = tempString.replacingOccurrences(of: "&deg;", with: "\u{00B0}")
        return result
    }
    
    func dateFormatter(date: Date, source: String) -> String {
        if source == "date" {
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE, MM/dd"
            let result = formatter.string(from: date)
            return result
        }else {
            let formatter = DateFormatter()
            formatter.dateFormat = "h:mm a"
            let result = formatter.string(from: date)
            return result
        }
    }
    
    func takeScreenshot(shouldSave: Bool) -> UIImage? {
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        return screenshotImage
    }
    
    @objc func addLocation(sender: UIButton) {
        let searchController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchCityViewController") as! SearchCityViewController
        searchController.parentController = self
        navigationController?.pushViewController(searchController, animated: true)
    }
    
    @IBAction func uploadImage(_ sender: Any) {
        let image = takeScreenshot(shouldSave: true)
        let alert = UIAlertController(title: "", message: "Please check your Photo Library", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        })
        let imageView = UIImageView(frame: CGRect(x: -32, y: Int(view.bounds.size.height / 4), width: (image?.cgImage?.width)! / 8, height: (image?.cgImage?.height)! / 8))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.layer.borderColor = UIColor.blue.cgColor
        imageView.layer.borderWidth = 2
        imageView.layer.masksToBounds = true
        alert.view.addSubview(imageView)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyWeatherCell", for: indexPath) as! DailyWeatherTableViewCell
        let weather = weathers[indexPath.row]
        let date = Date(timeIntervalSince1970: Double(weather.dateTime))
        cell.dateLabel.text = dateFormatter(date: date, source: "date")
        cell.minTemperatureLabel.text = degreeFormatter(degree: Int(weather.minTemperature))
        cell.maxTemperatureLabel.text = "/\(degreeFormatter(degree: Int(weather.maxTemperature)))"
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        prepareForUI(index: indexPath.row)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
