//
//  Constant.swift
//  ShowWeather
//
//  Created by Apit on 8/7/18.
//  Copyright © 2018 Apit. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Constant: NSObject {
    
    static let apiKey = "yBVIxqkbjAnlKnXgQjpDl5pyZL6NzXY4"
    
    struct API {
        private static let baseApi = "http://dataservice.accuweather.com/"
        static let autoCompleteCity = baseApi + "locations/v1/cities/autocomplete"
        static let dailyForecase = baseApi + "forecasts/v1/daily/5day/"
    }
    
    static var manager: SessionManager!
    static func GET(url: String, header: [String: String], parameter: [String: Any], success: @escaping (JSON) -> Void, failure: @escaping (Error) -> Void) {
        if manager == nil {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 30
            configuration.timeoutIntervalForResource = 30
            manager = SessionManager(configuration: configuration)
        }
        let request = manager.request(url, method: .get, parameters: parameter, encoding: URLEncoding.default, headers: header)
        request.responseJSON { (response) in
            switch response.result {
            case .success(let json):
                let jsonObject = JSON(json)
                success(jsonObject)
            case .failure(let error):
                failure(error)
            }
        }
    }
}

class Weather: NSObject {
    var temperature: Int!
    var minTemperature: Float!
    var maxTemperature: Float!
    var dateTime: Int!
    var category: String!
}

class City: NSObject {
    var cityId: String!
    var cityName: String!
    var region: String!
}
